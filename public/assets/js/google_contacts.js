var clientId = '562480669970-vai8kg7bkbtt1jlgkd65h2k6iun9f3mv.apps.googleusercontent.com';
var apiKey = 'AIzaSyCbZJrhxL8ANzJDUTRehpYpNQacxfdSicw';
var google_tokenPage;
var google_contact_i = 0;

$(document).on("click","#btnImportGoogleContacts", function(){
	google_handleSignInClick();
});


  function google_handleClientLoad() {
    // Loads the client library and the auth2 library together for efficiency.
    // Loading the auth2 library is optional here since `gapi.client.init` function will load
    // it if not already loaded. Loading it upfront can save one network request.
    gapi.load('client:auth2', google_initClient);
  }

  function google_initClient() {
    // Initialize the client with API key and People API, and initialize OAuth with an
    // OAuth 2.0 client ID and scopes (space delimited string) to request access.
    gapi.client.init({
        apiKey: apiKey,
        discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
        clientId: clientId,
        scope: 'https://www.googleapis.com/auth/contacts.readonly'
    }).then(function () {
      // Listen for sign-in state changes.
      gapi.auth2.getAuthInstance().isSignedIn.listen(google_updateSigninStatus);

      // Handle the initial sign-in state.
      google_updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    });
  }

  function google_updateSigninStatus(isSignedIn) {
    // When signin status changes, this function is called.
    // If the signin status is changed to signedIn, we make an API call.
    if (isSignedIn) {
      google_makeApiCall();
    }
  }

  function google_handleSignInClick(event) {
    // Ideally the button should only show up after gapi.client.init finishes, so that this
    // handler won't be called before OAuth is initialized.
    gapi.auth2.getAuthInstance().signIn();
  }

  function handleSignOutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
  }

  function google_makeApiCall() {

	gapi.client.people.people.connections.list({
	  'resourceName': 'people/me',
	  'requestMask.includeField': 'person.emailAddresses,person.phoneNumbers',
	  'pageSize': 2000,
	  'pageToken': google_tokenPage
	}).then(function(response) {
	  
	  var connections = response.result.connections;
	  
	  //Process contacts
      if (connections.length > 0) {
          for (i = 0; i < connections.length; i++) {
//              console.log(google_contact_i, connections[i]);
			  
			  if (Array.isArray(connections[i].emailAddresses)) {
			  	for (j = 0; j < connections[i].emailAddresses.length; j++) {
			  		var hash = contact_detail_hash(connections[i].emailAddresses[j].value);
			  		console.log(google_contact_i, connections[i].emailAddresses[j].value, hash);
			  	}
			  }
			  
			  if (Array.isArray(connections[i].phoneNumbers)) {
			  	for (j = 0; j < connections[i].phoneNumbers.length; j++) {
			  		var hash = contact_detail_hash(connections[i].phoneNumbers[j].value);
			  		console.log(google_contact_i, connections[i].phoneNumbers[j].value, hash);
			  	}
			  }
			  
			  google_contact_i++;
          }
      }
	  
	  //Process next page
	  if(typeof response.result.nextPageToken != 'undefined') {
	      google_tokenPage = response.result.nextPageToken;
	      google_makeApiCall();
	  }
	  
	}, function(reason) {
	  console.log('Error: ' + reason.result.error.message);
	});
  }